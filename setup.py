#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name='porownywarka_stx_allegro',
    version='0.2',
    author='Krzysztof Klinikowski',
    author_email='krzysztof.klinikowski@stxnext.pl',
    packages=['porownywarka_stx_allegro'],
    install_requires     = ['mechanize'],
    test_suite='nose.collector',
    tests_require=['nose'],
    # other arguments here...
    entry_points = {
        'console_scripts': [
            'allegro = porownywarka_stx_allegro.scripts:main',
        ],
    }
)