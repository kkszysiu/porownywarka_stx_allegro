#-*- coding: utf-8 -*-
import argparse
import porownywarka_stx_allegro.lib as allegro


def main():
    """
    Used to run from CLI
    """
    parser = argparse.ArgumentParser(
        description='Get cheapest item URL and price from allegro.pl')
    parser.add_argument('keyword', type=str, help='a keyword to search')
    args = parser.parse_args()

    data = allegro.allegro_request(args.keyword)

    print "Najtańszy przedmiot: %s, cena: %s zł, link: %s" % (
        data[0].encode('utf8'), data[1], data[2].encode('utf8'))

if __name__ == '__main__':
    main()
