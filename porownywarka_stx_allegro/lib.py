#-*- coding: utf-8 -*-
import urllib
import locale
import mechanize
from BeautifulSoup import BeautifulSoup

locale.setlocale(locale.LC_ALL, '')


class NotHTMLException(Exception):
    """ Raised when fetched page is not HTML-like """
    pass


class KeywordNotSpecifiedException(Exception):
    """ Raised when keyword is empty """
    pass


class ItemNotFoundException(Exception):
    """ Raised when item was not found on Allegro.pl """
    pass


def allegro_request(product_name):
    """
    Make request to Allegro
    """

    if not product_name:
        raise KeywordNotSpecifiedException("Keyword needs to be specified")

    base_url = "http://allegro.pl"

    brser = mechanize.Browser()
    brser.open(base_url)

    if not brser.viewing_html():
        raise NotHTMLException(
            "Fetched website source doesnt seem to be HTML-like")

    brser.select_form(nr=0)
    brser["string"] = product_name
    response2 = brser.submit()

    params = {"offerTypeBuyNow": 1,
              "standard_allegro": 1,
              "order": "d",
              "counters": 1}

    fullurl = response2.geturl() + "&" + urllib.urlencode(params)
    response3 = mechanize.urlopen(fullurl)

    soup = BeautifulSoup(response3.read())

    definitions = soup.findAll('article', {"class": 'offer'})

    if not definitions:
        raise ItemNotFoundException("Item not found on Allegro.pl")

    definition = definitions[0]

    price = definition.find("div", {"class": 'price'}) \
                      .find("span").contents[2].strip()
    price = locale.atof(price.replace(" ", ""))
    link = definition.find('header').find('a')
    url = base_url + link.get('href')
    name = link.find('span').string

    return name, price, url
