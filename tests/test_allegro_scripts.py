#-*- coding: utf-8 -*-
import urllib2
import argparse
from nose.tools import ok_, eq_, raises
import porownywarka_stx_allegro.lib as allegro
import porownywarka_stx_allegro.scripts as script

class FakeArgParser():
    def __init__(self):
        self.keyword = "laptop"

    def __call__(self, description=None):
        return self

    def add_argument(self, *args, **kwargs):
        pass

    def parse_args(self):
        return self

def test_main():
    script.argparse.ArgumentParser = argparse.ArgumentParser
    script.main()

@raises(urllib2.HTTPError)
def test_main_without_item():
    fakrargparser = FakeArgParser()
    fakrargparser.keyword = ""
    script.argparse.ArgumentParser = fakrargparser
    script.main()

def test_main_with_fakeargparse():
    script.argparse.ArgumentParser = FakeArgParser()
    script.main()

@raises(allegro.ItemNotFoundException)
def test_main_with_fakeargparse_with_nonexisting_item():
    fakrargparser = FakeArgParser()
    fakrargparser.keyword = "laptop_item_that_doesnt_exists_asasasas"
    script.argparse.ArgumentParser = fakrargparser
    script.main()
