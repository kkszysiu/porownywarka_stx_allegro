#-*- coding: utf-8 -*-
import socket
import urllib2
from StringIO import StringIO
from nose.tools import ok_, eq_, raises
import porownywarka_stx_allegro.lib as allegro

class FakeUrllib2Response():
    def __init__(self, xml):
        self.xml_response = xml

    def read(self):
        return self.xml_response


class FakeUrllib2():
    def __init__(self, xml):
        self.xml_response = xml

    def urlopen(self, url):
        return FakeUrllib2Response(self.xml_response)


class FakeUrllib2UrlError():
    def urlopen(self, url):
        raise urllib2.URLError("[Errno 111] Connection refused")


class FakeUrllib2HTTPError():
    def urlopen(self, url):
        raise urllib2.HTTPError("", 404, "File not found", "", StringIO())


class FakeUrllib2TimeoutError():
    def urlopen(self, url):
        raise socket.timeout("Connection timeout")

def test_allegro_request():
    allegro.allegro_request("laptop")

@raises(allegro.ItemNotFoundException)
def test_allegro_request():
    allegro.allegro_request("laptop_item_that_doesnt_exists_asasasas")
